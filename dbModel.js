import mongoose from "mongoose";

const concursoTalentosSchema = mongoose.Schema({
  title: String,
  description: String,
  autor: String,
  url_video: String,
  votes: String,
});

export default mongoose.model("concursoVideos", concursoTalentosSchema);
